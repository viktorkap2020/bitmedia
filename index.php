<?php

require_once __DIR__ . "/includes/Controller.class.php";
$controller = new Controller();
$action = $_REQUEST['action'] ?? 'index';

switch ($action)
{
    case 'index':
        $controller->actionIndex();
        break;
    case 'api-users':
        $controller->actionApiUsers();
        break;
    case 'api-statistic':
        $controller->actionApiStatistic();
        break;
    case 'id':
        $controller->actionProfile();
        break;
}