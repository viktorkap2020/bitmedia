# http://localhost:9000/?action=api-statistic - ошибка "укажите user_id"
# http://localhost:9000/?action=api-statistic&user_id=4 - ОК, непустой результат
# http://localhost:9000/?action=api-statistic&user_id=4000 - ОК, пустой результат
# http://localhost:9000/?action=api-statistic&user_id=1.5 - ОК, пустой результат
# http://localhost:9000/?action=api-statistic&user_id=a - ошибка "неверный формат user_id"
# http://localhost:9000/?action=api-statistic&user_id=4&date_from=2019-10-03 - ОК, непустой результат
# http://localhost:9000/?action=api-statistic&user_id=4&date_from=а - ошибка "неверная дата"
# http://localhost:9000/?action=api-statistic&user_id=4&date_from=2019-13-01 - ошибка "неверная дата"
# http://localhost:9000/?action=api-statistic&user_id=4&date_from=2019-11-31 - ошибка "неверная дата"

# Тесты по пагинации
# # http://localhost:9000/?action=api-users - ОК, 50 записей
# # http://localhost:9000/?action=api-users&page=-2 - номер страницы должен быть больше или равно 1
# # http://localhost:9000/?action=api-users&page=k - неверный номер страницы
