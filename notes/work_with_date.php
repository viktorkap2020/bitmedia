<?php

// Первое, с чем РНР работает в плане даты - это Unix-метки. Unix-метка - это целое число, которое означает кол-во секунд от 1970-01-01 00:00:00 до "сейчас".
//echo time() . "\n";

// Второе, чем РНР работает - это строки, в которых может быть записана дата. Строка превращается в Unix-метку с помощью функции strtotime()
//echo strtotime("2020-01-01 00:01:00") . "\n"; // 1577829660
//echo strtotime("2020-01-01 00:02:00") . "\n"; // 1577829720

// strtotime умеет целую кучу превращений, например:
//echo strtotime("+1 day") . "\n";
//echo strtotime("last day of previous month") . "\n";

// Если надо сделать обратное действие - превратить Unix-метку в строку - используем функцию date(string $format, int time)
// Здесь справка по форматам: https://www.php.net/manual/ru/datetime.format.php
//echo date('Y-m-d', time()) . "\n"; // 2020-10-15
//echo date('d.m.Y', time()) . "\n"; // 15.10.2020

// Валидировать дату (т.е. проверить, является ли строка корректной датой) можно несколькими способами.

// Способ 1: if (!empty(strtotime))
//if (!empty(strtotime('aaa'))) {echo "ok\n";} else {echo "error\n";}
//if (!empty(strtotime('2020-01-15'))) {echo "ok\n";} else {echo "error\n";}

// Недостаток первого способа - в том, что не указан формат:
//if (!empty(strtotime('+1 day'))) {echo "ok\n";} else {echo "error\n";}

// Способ 2: с указанием формата:
//$format = "Y-m-d";
//$str = '15.10.2020';
//if ($str == date($format, strtotime($str))) {echo "ok\n";} else {echo "error\n";}
//
//$str = '2020-10-15';
//if ($str == date($format, strtotime($str))) {echo "ok\n";} else {echo "error\n";}

// Способ 3: регулярки + функция checkdate()
//$str = '2020-09-30';
//$parts = explode('-', $str);
//if (preg_match('/^\d{4}-\d{2}-\d{2}$/', $str) && checkdate($parts[1], $parts[2], $parts[0])) {
//    echo "ok\n";
//} else {
//    echo "error\n";
//}

// Способ 4: формат + класс DateTime
$format = "Y-m-d";
//$str = '15.10.2020';
$str = '2020-10-15';
$dateTimeObj = new DateTime($str);
if ($str == $dateTimeObj->format($format)) {echo "ok\n";} else {echo "error\n";}
