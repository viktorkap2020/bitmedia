<?php
require_once __DIR__ . "/Model.class.php";
require_once __DIR__ . "/View.class.php";

class Controller
{
    private $model;
    private $view;

    public function __construct()
    {
        $this->model = new Model();
        $this->view = new View();
    }

    public function actionIndex()
    {
        $this->view->echoPage();
    }

    public function actionApiUsers()
    {
        $data = $this->model->getDataForApiUsers();
        $this->view->echoApiResult($data);
    }

    public function actionApiStatistic()
    {
        $data = $this->model->getDataForApiStatistic();
        $this->view->echoApiResult($data);
    }

    public function actionProfile()
    {
        $this->model->getId();
        $this->view->echoApiResult();
    }
}


