<?php
require_once __DIR__ . '/View.class.php';
require_once __DIR__ . '/Controller.class.php';
require_once __DIR__ . '/../sql/db/DbDecorator.class.php';

class Model
{
    private $view;
    private $db;
    private $date_from;
    private $date_to;
    private $page;
    public $id;

    public function __construct()
    {
        $this->view = new View;
        $this->db = DbDecorator::getInstance();
        $this->date_from = $_GET['date_from'];
        $this->date_to = $_GET['date_to'];
        $this->page = $_GET['page'] ?? 1;
        $this->id = $_GET['id'];
    }

    public function getDataFromJson()
    {
        $jsonUsers = file_get_contents(__DIR__ . '/../Json_info/users.json');
        $jsonStatistic = file_get_contents(__DIR__ . '/../Json_info/users_statistic.json');

        $this->putUserDataToSql($jsonUsers);
        $this->putStatisticDataToSql($jsonStatistic);
        $this->getTotalPageViews($jsonStatistic);
        $this->getTotalClicks($jsonStatistic);
    }

    public function putUserDataToSql($jsonUsers)
    {
        $this->db->exec("DELETE FROM users WHERE TRUE");

        $users = json_decode($jsonUsers, true);
        foreach ($users as $id => $user) {
            $quotedValues = [];
            foreach ($user as $value) {
                $quotedValues[] = $this->db->quote($value);
            }
            $columns = implode(', ', array_keys($user));
            $values = implode(', ', $quotedValues);

            $sql = "INSERT INTO users({$columns}) VALUES ({$values});";
            $this->db->exec($sql);
        }

    }

    public function putStatisticDataToSql($jsonStatistic)
    {
        $this->db->exec("DELETE FROM users_statistic WHERE TRUE");

        $statistic = json_decode($jsonStatistic, true);
        foreach ($statistic as $stat) {
            $quotedValues = [];
            foreach ($stat as $value) {
                $quotedValues[] = $this->db->quote($value);
            }
            $columns = implode(', ', array_keys($stat));
            $values = implode(', ', $quotedValues);

            $sql = "INSERT INTO users_statistic({$columns}) VALUES ({$values});";
            $this->db->exec($sql);
        }
    }

    public function getDataForApiUsers()
    {
        try {
            $this->usersExceptionErrors();

            $limitCount = ($this->page - 1) * 50;
            return [
                'status' => true,
                'data'   => $this->db->fetchAll("SELECT *, (SELECT sum(page_views) FROM users_statistic WHERE user_id=users.id) AS total_page_views, (SELECT sum(clicks) FROM users_statistic WHERE user_id=users.id) AS total_clicks FROM users LIMIT {$limitCount}, 50")
            ];
        } catch (Exception $e) {
            return [
                'status' => false,
                'data'   => $e->getMessage(),
            ];
        }

    }

    public function getDataForApiStatistic()
    {
        try {
            $this->throwExceptionIfIncorrectData();

            $sqlWhere = [
                "user_id={$_GET['user_id']}",
            ];

            if (!empty($this->date_from)) {
                $sqlWhere[] = "date >= {$this->db->quote($this->date_from)}";
            }

            if (!empty($this->date_to)) {
                $sqlWhere[] = "date <= {$this->db->quote($this->date_to)}";
            }

            $sql = "SELECT *
                    FROM users_statistic 
                    WHERE " . implode(' AND ', $sqlWhere);

            return [
                'status' => true,
                'data'   => $this->db->fetchAll($sql),
            ];
        } catch
        (Exception $e) {
            return [
                'status' => false,
                'data'   => $e->getMessage(),
            ];
        }
    }

    public function throwExceptionIfIncorrectData()
    {
        $format = "Y-m-d";
        if (isset($_GET['date_from'])) {
            $str = $this->date_from;

            if ($str != date($format, strtotime($str))) {
                throw new Exception("Указана неверная дата date_from");
            }
        }

        if (isset($_GET['date_to'])) {

            $str = $this->date_to;

            if ($str != date($format, strtotime($str))) {
                throw new Exception("Указана неверная дата date_to");
            }
        }

        $user_id = $_GET['user_id'];
        if (empty($user_id)) {
            throw new Exception('укажите user_id');
        }

        if (filter_var($user_id, FILTER_VALIDATE_INT) == false) {
            throw new Exception('неверный user_id');
        }
    }

    public function usersExceptionErrors()
    {
        if(empty($this->page))
        {
            $this->page = 1;
        }

        if(filter_var($this->page, FILTER_VALIDATE_INT) == false)
        {
            throw new Exception('неверный номер страницы');
        }

        if($this->page < 1)
        {
            throw new Exception("номер страницы должен быть больше или равно 1");
        }
    }

    public function getTotalPageViews($jsonStatistic)
    {
        $this->db->fetchOne("SELECT sum(page_views) FROM users_statistic WHERE user_id={};");
    }

    public function getTotalClicks($jsonStatistic)
    {
        $this->db->fetchOne("SELECT sum(clicks) FROM users_statistic WHERE user_id={};");
    }

    public function getId()
    {
        $this->id;
    }
}