<?php
require_once __DIR__ . "/../sql/db/DbDecorator.class.php";
class View
{
    private $db;

    public function __construct()
    {
        $this->db = DbDecorator::getInstance();
    }

    public function echoPage()
    {
        echo "
        <html lang='ru'>
<head>
    <!-- подключил бутстрап -->
    <link rel='stylesheet' href='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css'/>
    <!-- подключил jQuery -->
    <script
  src='https://code.jquery.com/jquery-3.5.1.js'
  integrity='sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc='
  crossorigin='anonymous'></script>
</head>
<body>

<h1>Список пользователей</h1>

<form class='form-inline'>
<div class='form-group mx-sm-3 mb-2'>
<input type='text' class='form-control' size='5' id='page_number_input'>
</div>
<button id='button' type='buttonq' class='btn btn-primary mb-2'>Перейти</button>
</form>



<div id='message'></div>

<table class='table table-striped table-dark'>
    <thead>
    <tr>
        <th scope='col'>id</th>
        <th scope='col'>first_name</th>
        <th scope='col'>last_name</th>
        <th scope='col'>email</th>
        <th scope='col'>gender</th>
        <th scope='col'>ip_address</th>
        <th scope='col'>total_clicks</th>
        <th scope='col'>total_page_views</th>
    </tr>
    </thead>
    <tbody id='table_body'>
    </tbody>   
</table>

<script src='/frontend.js'>
</script>
        ";
    }

    public function echoApiResult(array $data)
    {
        header('Content-Type: application/json');
        echo json_encode($data, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }

}