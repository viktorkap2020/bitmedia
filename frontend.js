function getData(pageNumber) {
    jQuery.ajax({
        url: '/?action=api-users&page=' + pageNumber,
        dataType: "json",
        success: function (response) {
            if (response['status'] === false) {
                jQuery('#message').attr('class', 'alert alert-danger').text(response['data']);
            }
            if (response['status'] === true) {
                jQuery('#message').attr('class', 'alert alert-success').text("Найдено 50 записей");
            }

            jQuery('#table_body tr').remove();

            response['data'].forEach(function (row) {
                var id = row.id;
                var link = '<a href="/?action=profile&id=row.id">' + id + '</a>';

                jQuery('#table_body').append('<tr>' + '<td>' + link + '</td>' + '<td>' + row['first_name'] + '</td>' + '<td>' + row['last_name'] + '</td>' + '<td>' + row['email'] + '</td>' + '<td>' + row['gender'] + '</td>' + '<td>' + row['ip_address'] + '</td>' + '<td>' + row['total_clicks'] + '</td>' + '<td>' + row['total_page_views'] + '</td>' + '</tr>');
            });
        }
    });
}

// В самом начале работы загружаем первую страницу
getData(1);

// Если пользователь выбрал страницу и нажал на кнопку "Перейти", загружаем нужную страницу
jQuery('#button').on('click', function () {
    pageNumber = jQuery('#page_number_input').val();
    getData(pageNumber);
});


