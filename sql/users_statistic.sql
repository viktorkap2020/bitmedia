CREATE TABLE `users_statistic`
(
    id INT NOT NULL,
    user_id INT,
    date DATE,
    page_views INT,
    clicks INT,
    FOREIGN KEY (user_id) REFERENCES users(id)
);