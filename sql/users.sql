CREATE TABLE `users`
(
    id               INT,
    first_name       VARCHAR(20),
    last_name        VARCHAR(20),
    email            VARCHAR(64),
    gender           VARCHAR(8),
    ip_address       VARCHAR(20),
    PRIMARY KEY (id)
);